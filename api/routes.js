const db = require('./db');
const handler = require('./handler');

module.exports = [
    {
        method: 'GET',
        path: '/items-list',
        handler: handler.getResponse(db.listItems)
    }
];