const keywords = require('./data/keywords');
const types = require('./data/types');
const listItems = require('./data/list-items');

module.exports = {
    keywords,
    types,
    listItems,
};