const jsonServer = require('json-server');
const serverConfig = require('./server-config');
const db = require('./db');
const routes = require('./routes');
const server = jsonServer.create();
const middlewares = jsonServer.defaults();
const namespace = '/api';
const router = jsonServer.router(db);

server.use(middlewares);
server.use(jsonServer.bodyParser);

routes.forEach(routeConfig => {
    const {path, method, handler} = routeConfig;
    server[method.toLowerCase()](namespace + path, handler);
});

server.use(namespace, router);

server.listen(serverConfig.port, () => {
    console.log('dev API runing');
})