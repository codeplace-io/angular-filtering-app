import { Filter } from 'src/app/shared/types/filter';

export const sortFilters: Filter[] = [
    {
        code: 'descending',
        name: 'Descending'
    },
    {
        code: 'ascending',
        name: 'Ascending'
    }
];
export const typeFilters: Filter[] = [
    {
        code: '',
        name: 'Get all',
    },
    {
        code: 'video',
        name: 'Video'
    },
    {
        code: 'audio',
        name: 'Audio'
    },
    {
        code: 'document',
        name: 'Document'
    },
    {
        code: 'image',
        name: 'Image'
    },
];
