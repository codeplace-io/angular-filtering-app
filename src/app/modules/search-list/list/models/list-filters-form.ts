import { FormGroup, FormControl } from '@angular/forms';

import { FilterParams, OrderType } from 'src/app/shared/types';

export class ListFiltersForm {

    public queryParamsToFormValues(params: any): FormGroup {
        return new FormGroup({
            query: new FormControl(params.query || null),
            order: new FormControl(params.order || null),
            type: new FormControl(params.type || null)
        });
    }

    public formValueToParams(formValue: { query: string, order: OrderType; type: string }): Partial<FilterParams> {
        const query = formValue.query || null;
        const order = formValue.order || null;
        const type = formValue.type || null;

        return {
            query,
            order,
            type
        };
    }
}
