import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { SearchListRoutingModule } from './search-list-routing.module';
import { ListComponent } from './list/list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ItemsListResolver } from './services/items-list.resolver';
import { ItemsListService } from './services/items-list.service';

@NgModule({
    declarations: [
        ListComponent
    ],
    imports: [
        CommonModule,
        SearchListRoutingModule,
        ReactiveFormsModule,
        SharedModule
    ],
    providers: [
        ItemsListResolver,
        ItemsListService
    ]
})
export class SearchListModule { }
