import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { ListItem } from 'src/app/shared/types';
import { ItemsListService } from './items-list.service';

@Injectable()
export class ItemsListResolver implements Resolve<Observable<ListItem[]>> {

    constructor(private service: ItemsListService) {}

    resolve(): Observable<ListItem[]> {
        return this.service.getAll();
    }
}
