import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

export abstract class AbstractRestService<T> {
    protected http: HttpClient;
    protected abstract resource: string;
    protected abstract unwrapResponse(response);

    public getAll(): Observable<T> {
        return this.http.get<T>(this.basePath).pipe(this.unwrapResponse);
    }

    protected get basePath() {
        return `${environment.basePath}/${this.resource}`;
    }
}
