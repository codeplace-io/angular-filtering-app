export interface ListItem {
    id: string;
    title: string;
    type: Type;
    keywords: Keyword[];
}

export interface Keyword {
    name: string;
    code: string;
}

export interface Type {
    name: string;
    code: string;
}
