import { ListItem } from '../types';

abstract class AbstractFilter {

    public abstract checkItem(item: ListItem): boolean;

    public filter(items: ListItem[]): ListItem[] {
        return items.filter(this.checkItem);
    }
}

abstract class AbstractLogicFilter extends AbstractFilter {

    constructor(protected filters: AbstractFilter[]) {
        super();
    }

    addFilter(filter: AbstractFilter): void {
        this.filters.push(filter);
    }
}

export class LogicOrFilter extends AbstractLogicFilter {

    public checkItem = (item: ListItem): boolean => {
        let logic = false;

        for (const filter of this.filters) {
            logic = logic || filter.checkItem(item);
        }

        return logic;
    }
}

export class LogicAndFilter extends AbstractLogicFilter {

    public checkItem = (item: ListItem): boolean => {
        let logic = true;

        for (const filter of this.filters) {
            logic = logic && filter.checkItem(item);
        }

        return logic;
    }
}

export class KeywordFilter extends AbstractFilter {

    constructor(private keywordCode: string) {
        super();
    }

    protected matchAnyKeyword(item: ListItem): boolean {
        for (const keyword of item.keywords) {
            if (keyword.code === this.keywordCode) {
                return true;
            }
        }

        return false;
    }

    public checkItem(item: ListItem): boolean {
        return this.matchAnyKeyword(item);
    }
}

export class PropertyFilter extends AbstractFilter {
    constructor(private property: string, private value: string) {
        super();
    }

    private sanitizeString(value: string): string {
        return value.toLowerCase().trim();
    }

    protected checkProperty(item: ListItem): boolean {
        const stringToSearch = this.sanitizeString(item[this.property]);
        return stringToSearch.indexOf(this.value) > -1;
    }

    public checkItem(item: ListItem): boolean {
        return this.checkProperty(item);
    }
}

export class NestedPropertyFilter extends AbstractFilter {
    constructor(private property: string, private nestedProperty: string, private value: string) {
        super();
    }

    protected checkProperty(item: ListItem): boolean {
        return item[this.property][this.nestedProperty] === this.value;
    }

    public checkItem(item: ListItem): boolean {
        return this.checkProperty(item);
    }
}