import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormValidationHelperService } from './services/form-validation-helper.service';
import { SearchFilterService } from './services/search-filter.service';

@NgModule({
    declarations: [],
    imports: [
        CommonModule
    ],
    providers: [
        FormValidationHelperService,
        SearchFilterService
    ],
    exports: [ ]
})
export class SharedModule { }
